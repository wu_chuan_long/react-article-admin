/* eslint-disable react-refresh/only-export-components */
import type { FC } from 'react'
import { useRef, useState, useMemo } from 'react'
import { Space, Button, Avatar, message } from 'antd'
import useUserStore, { selectAvatar } from '@/store/user-store'
import { useSubmit } from 'react-router-dom'
import type { ActionFunctionArgs } from 'react-router-dom'
import { updateAvatarApi } from '@/api/user-api.ts'
import to from 'await-to-js'
import { useNavSubmitting } from '@/utils/hooks'

const UserAvatar: FC = () => {
  const [avatarName, setAvatarName] = useState("")
  const avatar = useUserStore(selectAvatar)
  const [newAvatar, setNewAvatar] = useState('')
  const iptRef = useRef<HTMLInputElement>(null)
  const submit = useSubmit()
  const submitting = useNavSubmitting('PATCH')

  // 动态计算，并缓存计算的结果
  const isDisabled = useMemo(() => !newAvatar || newAvatar === avatar, [newAvatar, avatar])

  const showDialog = () => {
    iptRef.current?.click()
  }

  const onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.currentTarget.files
    if (!files || files.length === 0) return
    setAvatarName(files[0].name)

    // 创建文件读取器，把文件读为 base64 的字符串
    const fr = new FileReader()
    fr.readAsDataURL(files[0])
    fr.onload = () => {
      if (fr.result) {
        setNewAvatar(fr.result as string)
      }
    }
  }

  const saveAvatar = () => {
    if (submitting) return
    submit({ avatar: newAvatar, avatarName }, { method: 'PATCH' })
  }

  return (
    <Space direction="vertical">
      {/* 按需渲染头像组件 */}
      {newAvatar || avatar ? (
        <Avatar size={300} shape="square" src={newAvatar || avatar} />
      ) : (
        <Avatar size={300} shape="square" onClick={showDialog}>
          请选择头像
        </Avatar>
      )}

      <Space direction="horizontal">
        <Button onClick={showDialog}>选择照片</Button>
        <Button type="primary" disabled={isDisabled} loading={submitting && { delay: 200 }} onClick={saveAvatar}>
          保存头像
        </Button>
        <input type="file" accept="image/*" style={{ display: 'none' }} ref={iptRef} onChange={onFileChange} />
      </Space>
    </Space>
  )
}

export default UserAvatar


function base64ToFile(base64: any, filename = 'example.png') {
  // 将base64的数据部分提取出来
  const parts = base64.split(';base64,');
  const contentType = parts[0].split(':')[1];
  const raw = window.atob(parts[1]);
  const rawLength = raw.length;
  const uInt8Array = new Uint8Array(rawLength);

  for (let i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  // 创建Blob对象
  const blob = new Blob([uInt8Array], { type: contentType });

  // 创建File对象
  const file = new File([blob], `${filename}`, {
    type: contentType,
    lastModified: new Date().getTime(),
  });

  return file;
}

export const action = async ({ request }: ActionFunctionArgs) => {
  const fd = await request.formData()
  const data = new FormData();
  const avatarName = fd.get("avatarName") as string;
  for (const [key, value] of fd.entries()) {
    if (key === 'avatar') {
      // 这里的value是base64类型的，所以需要转为File类型，后端才好解析。
      data.append('avatar', base64ToFile(value, avatarName))
    }
  }

  const [err] = await to(updateAvatarApi(data))

  if (err) return null
  message.success('头像更新成功！')
  return null
}
