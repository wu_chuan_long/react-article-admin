/// <reference types="vite/client" />

// ---------- 请求体的数据类型 ----------

type RegForm = {
  username: string;
  password: string;
  repassword: string;
};

type LoginForm = Omit<RegForm, "repassword">;

type UserInfoForm = Pick<User, "id" | "email" | "nickname">;

type ResetPwdForm = {
  old_pwd: string;
  new_pwd: string;
  re_pwd: string;
};

type ArtCateAddForm = Omit<CateItem, "id">;

type ArticleAddForm = {
  title: string;
  cate_id: number;
  content: string;
  state: "草稿" | "已发布";
  cover_img: string | Blob;
  [x: string]: string | Blob;
};

type ArticleEditForm = ArticleAddForm & { readonly id: string, category: CateItem,file:{filename:string} };

type ArticleAddBaseForm = Partial<Pick<ArticleAddForm, "title" | "cate_id">>;
type ArticleEditBaseForm = ArticleAddBaseForm;

type ArtListQuery = {
  pagenum: number;
  pagesize: number;
  cate_id: number | string;
  state: string;
};

type cateListQuery = {
  pagenum: number = 1;
  pagesize: number = 10;
};

// ---------- 接口返回的数据类型 ----------

// 接口返回的数据的基础类型
interface BaseResponse<T = unknown> {
  code: number;
  msg: string;
  data?: {
    page: {
      total: number;
      pagenum: number;
      pagesize: number;
    },
    rows: T
  };
}

// 接口返回的数据的基础类型
interface FormResponse<T = unknown> {
  code: number;
  msg: string;
  data?: T
}

// 登录接口返回的数据类型
interface LoginResponse extends BaseResponse {
  data: {
    token?: string;
  };
}

// 文章列表接口返回的数据类型
interface ArticleListResponse extends BaseResponse<Article[]> {
  data?: {
    page: {
      total: number;
      pagenum: number;
      pagesize: number;
    },
    rows: T
  };
}

// 用户的基本信息
type User = {
  readonly id: number;
  username: string;
  nickname?: string;
  email?: string;
  user_pic?: string;
  avatar?:any
};

// 左侧菜单项的 TS 类型
type MenuItem = {
  readonly key: string;
  title?: string;
  label: string;
  icon: React.ReactNode;
  children?: MenuItem[];
};

// 文章分类 Item 的类型
type CateItem = {
  id: number;
  cate_name: string;
  cate_alias: string;
};

// 文章的类型
type Article = {
  readonly id: number;
  title: string;
  pub_date: string;
  state: "草稿" | "已发布";
  cate_name: string;
};
