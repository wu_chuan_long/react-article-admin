import axios from '@/api'

// 分类列表
export const getCateListApi = (data:cateListQuery={pagenum:1,pagesize:10}) => axios.get<null, BaseResponse<CateItem[]>>('/category',{params:data})

// 添加分类
export const postCateApi = (data: FormData) => axios.post<null, BaseResponse>('/category', data)

// 修改分类
export const editCateApi = (data: FormData) => axios.put<null, BaseResponse>('/category', data)

// 删除分类
export const delCateApi = (id:string) => axios.delete<null, BaseResponse>(`/category/${id}`)
