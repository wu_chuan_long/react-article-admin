import axios from '@/api'

// 发布文章
export const postArticleApi = (data:FormData) => axios.post<null, BaseResponse>('/article', data)

// 根据分页，获取文章的列表数据
export const getArticleListApi = (data: ArtListQuery) => axios.get<null, ArticleListResponse>('/article', { params: data })

// 删除文章
export const deleteArticleApi = (id: string) => axios.delete<null, BaseResponse>(`/article/${id}`)

// 根据 id 获取文章详情
export const getArticleApi = (id: string) => axios.get<null, FormResponse<ArticleEditForm>>(`/article/${id}`)

// 修改文章内容
export const putArticleApi = (data: FormData) => axios.put<null, BaseResponse>('/article', data)
